
USE [lin2world]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[user_daily_quest]') AND name = N'PK_user_daily_quest')
ALTER TABLE [dbo].[user_daily_quest] DROP CONSTRAINT [PK_user_daily_quest]ALTER TABLE [dbo].[user_daily_quest] ADD  CONSTRAINT [PK_user_daily_quest] PRIMARY KEY CLUSTERED 
(
	[char_id] ASC, [quest_id] ASC
)WITH (PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

USE [lin2world]
GO
/****** Object:  StoredProcedure [dbo].[lin_SetDailyQuest]    Script Date: 07/09/2017 11:14:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lin_SetDailyQuest] 
(
 @char_id int,
 @quest_id int,
 @complete_date int
)
AS

SET NOCOUNT ON

UPDATE user_daily_quest SET complete_date = @complete_date
WHERE char_id = @char_id AND quest_id = @quest_id

IF @@rowcount = 0
BEGIN
 INSERT INTO user_daily_quest (char_id, quest_id, complete_date) 
 VALUES (@char_id, @quest_id, @complete_date)
END

