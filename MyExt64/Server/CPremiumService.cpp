
#include <Server/CPremiumService.h>
#include <Server/CPremiumServiceSocket.h>
#include <Server/CShopServer.h>
#include <Server/CUser.h>
#include <Server/CUserSocket.h>
#include <Server/CItem.h>
#include <Common/Utils.h>
#include <Common/SmartPtr.h>
#include <Common/Config.h>
#include <Common/CSharedItemData.h>

void CPremiumService::Init()
{
	WriteMemoryBYTE(0xBE6100, 'c');
	WriteMemoryBYTE(0xBE61A8, 'c');
	WriteInstructionCall(0x9122A3, FnPtr(&CPremiumService::RequestGetGamePoint));
	WriteInstructionCall(0x912526, FnPtr(&CPremiumService::SendBuyItemRequest));
	WriteInstructionCall(0x485610, FnPtr(&CPremiumService::RequestSubGamePoint));
	WriteInstructionCall(0x4854C8, FnPtr(&CPremiumService::RequestAddGamePoint));
	WriteMemoryBYTES(0x7EAFEB, "\xB9\x10\x27\x00\x00\x90", 6); // mov ecx, 10000; nop - reconnect 10 seconds, not 3 hours :)
	WriteInstructionCall(0x7EB015, FnPtr(static_cast<void(*)()>(&CPremiumService::Bind)));
	WriteInstructionCall(0x7EB11C, FnPtr(static_cast<void(*)()>(&CPremiumService::Bind)));
}

void CPremiumService::Bind()
{
	CLog::Add(CLog::Blue, L"Premium Service Initializing");

	Bind(0x01, &CPremiumService::ReplyGetGamePoint);
	Bind(0x02, &CPremiumService::ReplyBuyItem);
	Bind(0x03, &CPremiumService::ReplySubGamePoint);
	Bind(0x04, &CPremiumService::ReplyNetPing);
	Bind(0x05, &CPremiumService::ReplyAddGamePoint);

	reinterpret_cast<void(*)()>(0x7EAFAC)();
}

CPremiumService* CPremiumService::Instance()
{
	return reinterpret_cast<CPremiumService*>(0x10BA2C10);
}

bool CPremiumService::IsShopPaused()
{
	return *reinterpret_cast<const bool*>(0xF0C7FF);
}

bool CPremiumService::ReplyNetPing(CPremiumServiceSocket *socket, const unsigned char *packet)
{
	int serverTick = 0;
	Disassemble(packet, "d", &serverTick);
	socket->Send("cd", 0x04, serverTick);
	return false;
}

bool CPremiumService::ReplyAddGamePoint(CPremiumServiceSocket *socket, const unsigned char *packet)
{
	int userId = 0, result = 0, type = 0;
	INT32 points = 0;
	Disassemble(packet, "dddd", &userId, &result, &type, &points);

	SmartPtr<CUser> user = SmartPtr<CUser>::FromDBID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return false;
	}
	if (result) {
		CLog::Add(CLog::Red, L"Wrong result [%d] for user [%d][%s] at [%s][%d]", result, userId, user->GetName(), __FILEW__, __LINE__);
		return false;
	}
	switch (type) {
	case PointDonate:
		user->premiumPoints = points;
		user->socket->Send("chdQd", 0xFE, GetOpcodeGamePoint(), user->GetDBID(), points, 0);
		break;
	case PointVote:
		user->ext.votePoints = points;
		break;
	default:
		CLog::Add(CLog::Red, L"Invalid point type [%d] for user [%d][%s] at [%s][%d]", type, userId, user->GetName(), __FILEW__, __LINE__);
	}
	return false;
}

bool CPremiumService::ReplySubGamePoint(CPremiumServiceSocket *socket, const unsigned char *packet)
{
	int userId = 0, result = 0, type = 0;
	INT32 points = 0;
	Disassemble(packet, "dddd", &userId, &result, &type, &points);

	SmartPtr<CUser> user = SmartPtr<CUser>::FromDBID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return false;
	}
	if (result) {
		CLog::Add(CLog::Red, L"Wrong result [%d] for user [%d][%s] at [%s][%d]", result, userId, user->GetName(), __FILEW__, __LINE__);
		return false;
	}
	switch (type) {
	case PointDonate:
		user->premiumPoints = points;
		user->socket->Send("chdQd", 0xFE, GetOpcodeGamePoint(), user->GetDBID(), points, 0);
		break;
	case PointVote:
		user->ext.votePoints = points;
		break;
	default:
		CLog::Add(CLog::Red, L"Invalid point type [%d] for user [%d][%s] at [%s][%d]", type, userId, user->GetName(), __FILEW__, __LINE__);
	}
	return false;
}

bool CPremiumService::ReplyBuyItem(CPremiumServiceSocket* pSocket, const unsigned char* packet)
{
	int userId = 0, result = 0;
	INT32 productId = 0, count = 0, pointsLeft = 0;
	Disassemble(packet, "ddddd", &userId, &result, &productId, &count, &pointsLeft);

	SmartPtr<CUser> user = SmartPtr<CUser>::FromDBID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return false;
	}
	user->EndBuyProduct();
	switch (result) {
	case 0:
		{
			if (user->IsNowTrade()) user->TradeCancel();
			SmartPtr<CProduct> product = CShopServer::Instance()->GetProduct(productId);
			if (!product) {
				CLog::Add(CLog::Red, L"Invalid productId [%d] for user [%d][%s] at [%s][%d]", productId, userId, user->GetName(), __FILEW__, __LINE__);
				user->socket->Send("chd", 0xFE, GetOpcodeBuyProduct(), -2);
				return false;
			}
			for (int i = 0; i < count; ++i) {
				for (std::vector<CProduct::ItemData>::const_iterator idata = product->data.begin(); idata != product->data.end(); ++idata) {
					if (!user->inventory.CheckAddable(idata->itemId, idata->count, false)) {
						CLog::Add(CLog::Red, L"Cannot add item [%d][%d] to user [%d][%s] inventory at [%s][%d]",
							idata->itemId, idata->count, userId, user->GetName(), __FILEW__, __LINE__);
						continue;
					}
					user->AddItemToInventory(idata->itemId, idata->count);
				}
			}
			int price = product->price * count;
			user->socket->Send("chd", 0xFE, GetOpcodeBuyProduct(), 1);
			Instance()->socket->Send("cdddd", 0x03, user->socket->accountId, user->GetDBID(), PointDonate, price);
		}
		break;
	case 3:
		user->socket->Send("chd", 0xFE, GetOpcodeBuyProduct(), -1);
		break;
	default:
		CLog::Add(CLog::Red, L"Wrong result [%d] for user [%d][%s] at [%s][%d]", result, userId, user->GetName(), __FILEW__, __LINE__);
	}
	return false;
}

bool CPremiumService::ReplyGetGamePoint(CPremiumServiceSocket *pSocket, const unsigned char *packet)
{
	int userId = 0, error = 0, type = 0, points = 0;
	Disassemble(packet, "dddd", &userId, &error, &type, &points);

	if (!userId) return false;
	SmartPtr<CUser> user = SmartPtr<CUser>::FromDBID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return false;
	}
	switch (type) {
	case PointDonate:
		user->premiumPoints = points;
		user->socket->Send("chdQd", 0xFE, GetOpcodeGamePoint(), user->GetDBID(), points, PointDonate);
		break;
	case PointVote:
		user->ext.votePoints = points;
		break;
	default:
		CLog::Add(CLog::Red, L"Invalid point type [%d] for user [%d][%s] at [%s][%d]", type, userId, user->GetName(), __FILEW__, __LINE__);
	}

	return false;
}

bool CPremiumService::Bind(int id, bool(*function)(CPremiumServiceSocket*, const unsigned char*))
{
	return reinterpret_cast<bool(*)(INT64, int, bool(*)(CPremiumServiceSocket*, const unsigned char*))>(0x7E9A64)(0x10BA2CC0, id, function);
}

int CPremiumService::GetOpcodeBuyProduct()
{
	switch (Config::GetProtocolVersion()) {
	case Config::ProtocolVersionGraciaFinalUpdate1:
		return 0xAA;
	case Config::ProtocolVersionGraciaEpilogue:
	case Config::ProtocolVersionGraciaEpilogueUpdate1:
		return 0xBB;
	default:
		return 0xA9;
	}
}

int CPremiumService::GetOpcodeGamePoint()
{
	switch (Config::GetProtocolVersion()) {
	case Config::ProtocolVersionGraciaFinalUpdate1:
		return 0xA7;
	case Config::ProtocolVersionGraciaEpilogue:
	case Config::ProtocolVersionGraciaEpilogueUpdate1:
		return 0xB8;
	default:
		return 0xA6;
	}
}

void CPremiumService::RequestAddGamePoint(int userId, INT64 points)
{
	if (!connected || !verified || !userId || IsShopPaused()) return;
	SmartPtr<CUser> user = SmartPtr<CUser>::FromObjectID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return;
	}
	if (user->IsNowTrade()) user->TradeCancel();
	int premiumShopItemId = Config::Instance()->custom->premiumShopItemId;
	if (!premiumShopItemId) {
		socket->Send("cdddd", 0x05, user->socket->accountId, user->GetDBID(), PointDonate, points);
		return;
	}
	INT64 leftPoints = 0;
	if (CItem *item = user->inventory.GetFirstItemByClassID(premiumShopItemId, false)) {
		leftPoints = item->sd->count;
	}
	leftPoints += points;
	user->AddItemToInventory(premiumShopItemId, points);
	user->socket->Send("chdQd", 0xFE, GetOpcodeGamePoint(), user->GetDBID(), leftPoints, 0);
}

void CPremiumService::RequestSubGamePoint(int userId, INT64 points)
{
	if (!connected || !verified || !userId || IsShopPaused()) return;
	SmartPtr<CUser> user = SmartPtr<CUser>::FromObjectID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return;
	}
	if (user->IsNowTrade()) user->TradeCancel();
	int premiumShopItemId = Config::Instance()->custom->premiumShopItemId;
	if (!premiumShopItemId) {
		socket->Send("cdddd", 0x05, user->socket->accountId, user->GetDBID(), PointDonate, points);
		return;
	}
	INT64 leftPoints = 0;
	if (CItem *item = user->inventory.GetFirstItemByClassID(premiumShopItemId, false)) {
		if (item->sd->count < points) {
			CLog::Add(CLog::Red, L"Not enough points [%d][%d] for user [%d][%s] at [%s][%d]",
				item->sd->count, points, userId, user->GetName(), __FILEW__, __LINE__);
		} else {
			leftPoints = item->sd->count - points;
			user->DeleteItemInInventory(premiumShopItemId, points);
		}
	}
	user->socket->Send("chdQd", 0xFE, GetOpcodeGamePoint(), user->GetDBID(), leftPoints, 0);
}

int CPremiumService::SendBuyItemRequest(int userId, int productId, int count)
{
	if (!connected || !verified || !userId || IsShopPaused()) return -6;
	SmartPtr<CUser> user = SmartPtr<CUser>::FromObjectID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return -9;
	}
	SmartPtr<CProduct> product = CShopServer::Instance()->GetProduct(productId);
	if (!product) {
		CLog::Add(CLog::Red, L"Invalid productId [%d] for user [%d][%s] at [%s][%d]", productId, userId, user->GetName(), __FILEW__, __LINE__);
		return -2;
	}
	if (count <= 0 || count >= 100) {
		CLog::Add(CLog::Red, L"Invalid product count [%d] for user [%d][%s] at [%s][%d]", count, userId, user->GetName(), __FILEW__, __LINE__);
		return -2;
	}
	int premiumShopItemId = Config::Instance()->custom->premiumShopItemId;
	if (!premiumShopItemId) {
		socket->Send("cddddd", 0x02, user->socket->accountId, user->GetDBID(), productId, count, product->price);
		return 1;
	}
	INT64 price = product->price * count;
        if (!user->inventory.HaveItemByClassId(premiumShopItemId, price, false)) {
		user->EndBuyProduct();
		user->socket->Send("chd", 0xFE, GetOpcodeBuyProduct(), -2);
		return 1;
	}
	user->EndBuyProduct();
	if (count <= 0) return 1;
	if (user->IsNowTrade()) user->TradeCancel();
	for (int i = 0 ; i < count ; ++i) {
		for (std::vector<CProduct::ItemData>::const_iterator idata = product->data.begin() ; idata != product->data.end() ; ++idata) {
			if (!user->inventory.CheckAddable(idata->itemId, idata->count, false)) {
				CLog::Add(CLog::Red, L"Cannot add item [%d][%d] to user [%d][%s] inventory at [%s][%d]",
					idata->itemId, idata->count, userId, user->GetName(), __FILEW__, __LINE__);
				continue;
			}
			user->AddItemToInventory(idata->itemId, idata->count);
		}
	}
	user->socket->Send("chd", 0xFE, GetOpcodeBuyProduct(), 1);
	user->DeleteItemInInventory(premiumShopItemId, price);
	user->premiumPoints -= price;
	return 1;
}

void CPremiumService::RequestGetGamePoint(int userId, int type)
{
	if (!connected || !verified || !userId || IsShopPaused()) return;
	SmartPtr<CUser> user = SmartPtr<CUser>::FromObjectID(userId);
	if (!user) {
		CLog::Add(CLog::Red, L"User [%d] not found at [%s][%d]", userId, __FILEW__, __LINE__);
		return;
	}
	int premiumShopItemId = Config::Instance()->custom->premiumShopItemId;
	if (!premiumShopItemId) {
		user->premiumPoints = 0;
		socket->Send("cddd", 0x01, user->socket->accountId, user->GetDBID(), PointDonate);
		return;
	}
	CItem *item = user->inventory.GetFirstItemByClassID(premiumShopItemId, false);
	INT64 points = 0;
	if (item) points = item->sd->count;
	user->premiumPoints = points;
	user->socket->Send("chdQd", 0xFE, GetOpcodeGamePoint(), user->GetDBID(), points, 0);
}

