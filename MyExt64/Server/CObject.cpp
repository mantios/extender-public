
#include <Server/CObject.h>
#include <Common/Utils.h>

CObject* CObject::GetObject(const UINT32 objectId)
{
	return reinterpret_cast<CObject*(*)(const UINT32)>(0x75A8D8)(objectId);
}

void CObject::Delete()
{
	GetVfn<void(*)(CObject*)>(this, 0x1F)(this);
}

int CObject::GetDBID()
{
	return GetVfn<int(*)(CObject*)>(this, 0x8C)(this);
}

void CObject::SetDBID(const int id)
{
	GetVfn<void(*)(CObject*, int)>(this, 0x8D)(this, id);
}

bool CObject::IsItem()
{
	return GetVfn<bool(*)(CObject*)>(this, 0x47)(this);
}

