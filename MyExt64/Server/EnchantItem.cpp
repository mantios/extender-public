
#include <Server/EnchantItem.h>
#include <Server/CItem.h>
#include <Common/Parser.h>
#include <Common/Utils.h>
#include <Common/CLog.h>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

namespace {

std::map<int, double> chancesFighterWeapon;
std::map<int, double> chancesMageWeapon;
std::map<int, double> chancesNormalArmor;
std::map<int, double> chancesOnePieceArmor;
std::map<int, double> chancesEventWeapon;

class ParserImpl : public Parser {
public:
	ParserImpl() : lastMap(&chancesFighterWeapon), lastLevel(1)
	{
		weapon %= qi::lexeme[wide::string(L"weapon")];
		armor %= qi::lexeme[wide::string(L"armor")];
		fighter %= qi::lexeme[wide::string(L"fighter")][boost::lambda::var(lastMap) = &chancesFighterWeapon];
		mage %= qi::lexeme[wide::string(L"mage")][boost::lambda::var(lastMap) = &chancesMageWeapon];
		event %= qi::lexeme[wide::string(L"event")][boost::lambda::var(lastMap) = &chancesEventWeapon];
		normal %= qi::lexeme[wide::string(L"normal")][boost::lambda::var(lastMap) = &chancesNormalArmor];
		onepiece %= qi::lexeme[wide::string(L"onepiece")][boost::lambda::var(lastMap) = &chancesOnePieceArmor];
		level %= qi::lexeme[wide::string(L"level")];
		chance %= qi::lexeme[wide::string(L"chance")];
		weaponAssignment %= weapon > assign > (fighter | mage | event);
		armorAssignment %= armor > assign > (normal | onepiece);
		levelAssignment %= level > assign > integer[boost::lambda::var(lastLevel) = boost::lambda::_1 - 1];
		chanceAssignment %= chance > assign > decimal[boost::lambda::bind(&std::map<int, double>::operator[], boost::lambda::var(lastMap), boost::lambda::var(lastLevel)) = boost::lambda::_1];
		assignment %= weaponAssignment | armorAssignment | levelAssignment | chanceAssignment;
		start %= -bom > *assignment > qi::eoi;
	}

	Keyword weapon, armor, fighter, mage, event, normal, onepiece, level, chance;
	Rule assignment, weaponAssignment, armorAssignment, levelAssignment, chanceAssignment;
	std::map<int, double> *lastMap;
	int lastLevel;
};

} // namespace

void EnchantItem::Init()
{
	WriteMemoryBYTES(0x693391 + 0x0, "\x48\x89\xf9", 3); // mov rcx, rdi
	WriteMemoryBYTES(0x693391 + 0x3, "\x48\x89\xea", 3); // mov rdx, rbp
	WriteInstructionCall(0x693391 + 0x6, reinterpret_cast<UINT32>(GetFighterWeaponChance)); // get chance
	WriteMemoryBYTES(0x693391 + 0xB, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteInstructionJmp(0x693391 + 0xF, 0x6933AF, 0); // jump to 6933AF

	WriteMemoryBYTES(0x693308 + 0x0, "\x48\x89\xf9", 3); // mov rcx, rdi
	WriteMemoryBYTES(0x693308 + 0x3, "\x48\x89\xea", 3); // mov rdx, rbp
	WriteInstructionCall(0x693308 + 0x6, reinterpret_cast<UINT32>(GetMageWeaponChance)); // get chance
	WriteMemoryBYTES(0x693308 + 0xB, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteInstructionJmp(0x693308 + 0xF, 0x693326, 0); // jump to 693326

	NOPMemory(0x69341D, 6); // nop out jump if current armor enchant level < 3 -> success
	NOPMemory(0x69342E, 2); // nop out jump if current armor enchant level > 20 -> fail

	WriteMemoryBYTES(0x693453 + 0x0, "\x48\x89\xf9", 3); // mov rcx, rdi
	WriteInstructionCall(0x693453 + 0x3, reinterpret_cast<UINT32>(GetNormalArmorChance)); // get chance
	WriteMemoryBYTES(0x693453 + 0x8, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteInstructionJmp(0x693453 + 0xC, 0x69348A, 0); // jump to 69348A

	WriteMemoryBYTES(0x693435 + 0x0, "\x48\x89\xf9", 3); // mov rcx, rdi
	WriteInstructionCall(0x693435 + 0x3, reinterpret_cast<UINT32>(GetOnePieceArmorChance)); // get chance
	WriteMemoryBYTES(0x693435 + 0x8, "\xf2\x0f\x10\xf0", 4); // movsd xmm6, xmm0
	WriteInstructionJmp(0x693435 + 0xC, 0x69348A, 0); // jump to 69348A
}

void EnchantItem::Load()
{
	CLog::Add(CLog::Blue, L"Reading ..\\Script\\itemenchant.txt");
	if (!ParserImpl().Parse(L"..\\Script\\itemenchant.txt", true)) {
		CLog::Add(CLog::Red, L"Failed to load itemenchant.txt");
	} else {
		CLog::Add(CLog::Blue, L"Loaded ..\\Script\\itemenchant.txt");
	}
}

double __cdecl EnchantItem::GetFighterWeaponChance(const int level, CItem *item)
{
	GUARDED;

	if (item->itemInfo->crystalType == 8) return GetEventWeaponChance(level);
	std::map<int, double>::const_iterator i(chancesFighterWeapon.find(level));
	if (i == chancesFighterWeapon.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant fighter weapon +%d -> +%d not found, assuming zero chance", level, level+1);
		return 0.0;
	} else {
		return i->second;
	}
}

double __cdecl EnchantItem::GetMageWeaponChance(const int level, CItem *item)
{
	GUARDED;

	if (item->itemInfo->crystalType == 8) return GetEventWeaponChance(level);
	std::map<int, double>::const_iterator i(chancesMageWeapon.find(level));
	if (i == chancesMageWeapon.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant mage weapon +%d -> +%d not found, assuming zero chance", level, level+1);
		return 0.0;
	} else {
		return i->second;
	}
}

double __cdecl EnchantItem::GetNormalArmorChance(const int level)
{ GUARDED

	std::map<int, double>::const_iterator i(chancesNormalArmor.find(level));
	if (i == chancesNormalArmor.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant normal armor +%d -> +%d not found, assuming zero chance", level, level+1);
		return 0.0;
	} else {
		return i->second;
	}
}


double __cdecl EnchantItem::GetOnePieceArmorChance(const int level)
{ GUARDED

	std::map<int, double>::const_iterator i(chancesOnePieceArmor.find(level));
	if (i == chancesOnePieceArmor.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant one piece armor +%d -> +%d not found, assuming zero chance", level, level+1);
		return 0.0;
	} else {
		return i->second;
	}
}

double __cdecl EnchantItem::GetEventWeaponChance(const int level)
{
	GUARDED;

	std::map<int, double>::const_iterator i(chancesEventWeapon.find(level));
	if (i == chancesEventWeapon.end()) {
		CLog::Add(CLog::Red, L"Chance for enchant event weapon +%d -> +%d not found, assuming zero chance", level, level + 1);
		return 0.0;
	} else {
		return i->second;
	}
}

