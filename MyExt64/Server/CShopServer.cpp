
#include <Server/CShopServer.h>

CShopServer* CShopServer::Instance()
{
	return reinterpret_cast<CShopServer*>(0xF04A30);
}

SmartPtr<CProduct> CShopServer::GetProduct(int productId)
{
	SmartPtr<CProduct> result;
	reinterpret_cast<void(*)(CShopServer*, SmartPtr<CProduct>&, int)>(0x47642C)(this, result, productId);
	return result;
}

