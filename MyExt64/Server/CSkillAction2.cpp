
#include <Server/CSkillAction2.h>
#include <Common/Utils.h>

CSkillAction2* CSkillAction2::lastSkillActions[16];

void CSkillAction2::Init()
{
	WriteInstructionCall(0x715C34 + 0x364, FnPtr(&CSkillAction2::Activate));
	WriteInstructionCall(0x715C34 + 0x4DA, FnPtr(&CSkillAction2::Activate));
	WriteInstructionCall(0x715C34 + 0x7EA, FnPtr(&CSkillAction2::Activate));
}

bool CSkillAction2::Activate(class CCreature *caster, class CObject *target, int effectActivateTiming)
{
	lastSkillActions[GetThreadIndex()] = this;
	return reinterpret_cast<bool(*)(class CSkillAction2*, class CCreature*, class CObject *, int)>(
		0x713ED0)(this, caster, target, effectActivateTiming);
}

