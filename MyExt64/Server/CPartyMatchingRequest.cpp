
#include <Server/CPartyMatchingRequest.h>
#include <Server/CPartyRoomRegistry.h>
#include <Server/CUser.h>
#include <Server/CPartyRoom.h>
#include <Server/CPartyRoomInfo.h>
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>

void CPartyMatchingRequest::Init()
{
	WriteInstructionCall(0x9143D5, FnPtr(&CPartyMatchingRequest::OustFromRoom));
}

void CPartyMatchingRequest::ResponseOnSystemMessage(SmartPtr<CUser> user, int messageNo)
{
	reinterpret_cast<void(*)(CPartyMatchingRequest*, SmartPtr<CUser>*, int)>(0x7857B8)(this, &user, messageNo);
}

bool CPartyMatchingRequest::WithdrawRoomInternal(SmartPtr<CUser> user)
{
	return reinterpret_cast<bool(*)(CPartyMatchingRequest*, SmartPtr<CUser>*)>(0x78B998)(this, &user);
}

bool CPartyMatchingRequest::OustFromRoom(SmartPtr<CUser> user)
{
	GUARDED;

	if (!user) return false;
	CPartyRoomRegistry *registry = CPartyRoomRegistry::Instance();
	if (registry->IsPartyRoomMaster(user)) return false;
	if (user->sd->partyId && registry->IsPartyRoomMember(user)) {
		SmartPtr<CPartyRoom> room = registry->FindByMember(user);
		if (room) {
			SmartPtr<CUser> master = room->GetInfo()->GetMaster();
			ResponseOnSystemMessage(master, 1699);
		}
		return false;
	}
	if (!WithdrawRoomInternal(user)) return false;
	ResponseOnSystemMessage(user, 1393);
	return true;
}

