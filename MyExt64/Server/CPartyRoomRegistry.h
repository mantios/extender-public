
#pragma once

#include <Common/SmartPtr.h>

class CUser;
class CPartyRoom;

class CPartyRoomRegistry {
public:
	static CPartyRoomRegistry* Instance();

	bool IsPartyRoomMaster(SmartPtr<CUser> user);
	bool IsPartyRoomMember(SmartPtr<CUser> user);
	SmartPtr<CPartyRoom> FindByMember(SmartPtr<CUser> user);
};

