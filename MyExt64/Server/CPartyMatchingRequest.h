
#pragma once

#include <Common/SmartPtr.h>

class CUser;

class CPartyMatchingRequest {
public:
	static void Init();

	void ResponseOnSystemMessage(SmartPtr<CUser> user, int messageNo);
	bool WithdrawRoomInternal(SmartPtr<CUser> user);
	bool CPartyMatchingRequest::OustFromRoom(SmartPtr<CUser> user);
};

