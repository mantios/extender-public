
#include <Common/CYieldLock.h>
#include <Common/Utils.h>

CYieldLock::CYieldLock()
{
}

CYieldLock::~CYieldLock()
{
}

void CYieldLock::Enter(const wchar_t *filename, const unsigned int line)
{
	reinterpret_cast<void(__thiscall*)(CYieldLock*, const wchar_t*, const unsigned int)>(0x97E9D4)(this, filename, line);
}

void CYieldLock::Leave(const wchar_t *filename, const unsigned int line)
{
	reinterpret_cast<void(__thiscall*)(CYieldLock*, const wchar_t*, const unsigned int)>(0x97EA74)(this, filename, line);
}

CYieldLockGuard::CYieldLockGuard(CYieldLock *lock)
	: lock(lock), locked(true)
{
	lock->Enter(__FILEW__, __LINE__);
}

CYieldLockGuard::~CYieldLockGuard()
{
	if (locked) {
		lock->Leave(__FILEW__, __LINE__);
	}
}

void CYieldLockGuard::Lock()
{
	if (locked) return;
	lock->Enter(__FILEW__, __LINE__);
	locked = true;
}

void CYieldLockGuard::Unlock()
{
	if (!locked) return;
	lock->Leave(__FILEW__, __LINE__);
	locked = false;
}

