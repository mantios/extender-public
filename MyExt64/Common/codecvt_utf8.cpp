
#include <Common/codecvt_utf8.h>
#include <Windows.h>

codecvt_utf8::result codecvt_utf8::do_in(mbstate_t&,
	const char *from, const char *fromEnd, const char *&fromNext,
	wchar_t *to, wchar_t *toEnd, wchar_t *&toNext) const
{
	fromNext = from;
	toNext = to;
	for (;;) {
		const char *oldFromNext = fromNext;
		if (fromNext == fromEnd) return ok;
		if (toNext == toEnd) return partial;
		int c = static_cast<unsigned char>(*fromNext++);
		size_t chars(0);
		if (c < 0x80) {
			*toNext = c;
		} else if (c < 0xC0) {
			return error;
		} else if (c >= 0xFC) {
			*toNext = c & 0x01;
			chars = 5;
		} else if (c >= 0xF8) {
			*toNext = c & 0x03;
			chars = 4;
		} else if (c >= 0xF0) {
			*toNext = c & 0x07;
			chars = 3;
		} else if (c >= 0xE0) {
			*toNext = c & 0x0F;
			chars = 2;
		} else {
			*toNext = c & 0x1F;
			chars = 1;
		}
		for (; chars; --chars) {
			if (fromNext == fromEnd) {
				fromNext = oldFromNext;
				return partial;
			}
			c = static_cast<unsigned char>(*fromNext++);
			*toNext <<= 6;
			*toNext |= c & 0x3f;
		}
		++toNext;
	}
}

codecvt_utf8::result codecvt_utf8::do_out(mbstate_t&,
	const wchar_t *from, const wchar_t *fromEnd, const wchar_t *&fromNext,
	char *to, char *toEnd, char *&toNext) const
{
	fromNext = from;
	toNext = to;
	for (;;) {
		if (fromNext == fromEnd) return ok;
		UINT16 c = *fromNext++;
		if (c <= 0x7f) {
			if (toNext >= toEnd) return partial;
			*toNext++ = c;
		} else if (c <= 0x7ff) {
			if (toNext >= toEnd - 1) return partial;
			*toNext++ = (c >> 6) | 0xc0;
			*toNext++ = (c & 0x3f) | 0x80;
		} else {
			if (toNext >= toEnd - 2) return partial;
			*toNext++ = (c >> 12) | 0xe0;
			*toNext++ = ((c >> 6) & 0x3f) | 0x80;
			*toNext++ = (c & 0x3f) | 0x80;
		}
	}
}

bool codecvt_utf8::do_always_noconv() const throw()
{
	return false;
}

int codecvt_utf8::do_encoding() const throw()
{
	return 0;
}

