
#include <NPCd/Functions/CNPC_AddFollowDesire2.h>
#include <Common/CLog.h>

CNPC_AddFollowDesire2::CNPC_AddFollowDesire2() :
	NPCFunction(L"AddFollowDesire2", &AddFollowDesire2)
{
}

void* CNPC_AddFollowDesire2::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*, void*, void*, void*, void*)>(functionPtr.functionPtr)(
		caller, params[0], params[1], params[2], params[3], params[4]);
}

void CNPC_AddFollowDesire2::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_CREATURE);
	AddParameter(Type::TYPE_INT);
	AddParameter(Type::TYPE_INT);
	AddParameter(Type::TYPE_FLOAT);
	AddParameter(Type::TYPE_FLOAT);
}

int CNPC_AddFollowDesire2::AddFollowDesire2(CNPC *npc, CSharedCreatureData *creature, int value, int followType, float minDistance, float maxDistance)
{
	// just call AddFollowDesire without additional parameters for now
	return reinterpret_cast<int(*)(CNPC*, CSharedCreatureData*, int)>(0x49C3A8)(npc, creature, value);
}

