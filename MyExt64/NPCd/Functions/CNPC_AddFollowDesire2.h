
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>
#include <Common/CSharedCreatureData.h>

class CNPC_AddFollowDesire2 : public NPCFunction {
public:
	CNPC_AddFollowDesire2();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int AddFollowDesire2(CNPC *npc, CSharedCreatureData *creature, int value, int followType, float minDistance, float maxDistance);
};

