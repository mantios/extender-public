
#include <NPCd/Functions/CNPC_SetLocalMap.h>
#include <Common/CLog.h>

CNPC_SetLocalMap::CNPC_SetLocalMap() :
	NPCFunction(L"SetLocalMap", &SetLocalMap)
{
}

void* CNPC_SetLocalMap::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*, void*, void*)>(functionPtr.functionPtr)(
		caller, params[0], params[1], params[2]);
}

void CNPC_SetLocalMap::SetTypes()
{
	SetReturnType(Type::TYPE_VOID);
	AddParameter(Type::TYPE_INT);
	AddParameter(Type::TYPE_INT);
	AddParameter(Type::TYPE_INT);
}

int CNPC_SetLocalMap::SetLocalMap(CNPC *npc, int mapId, int key, int value)
{
	ScopedLock lock(npc->ext.localMapCS);
	npc->ext.localMap[mapId][key] = value;
	return 0;
}

