
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>
#include <Common/CSharedCreatureData.h>

class CNPC_IsPvpOrPk : public NPCFunction {
public:
	CNPC_IsPvpOrPk();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int IsPvpOrPk(CNPC *npc, CSharedCreatureData *c);
};

