
#pragma once

#include <NPCd/NPCFunction.h>
#include <NPCd/CNPC.h>
#include <Common/CSharedCreatureData.h>

class CNPC_ClearContributeData : public NPCFunction {
public:
	CNPC_ClearContributeData();
	virtual void* Call(void *caller, void **params);
	virtual void SetTypes();
	static int ClearContributeData(CNPC *npc);
};

