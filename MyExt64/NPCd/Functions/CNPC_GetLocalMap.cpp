
#include <NPCd/Functions/CNPC_GetLocalMap.h>
#include <Common/CLog.h>

CNPC_GetLocalMap::CNPC_GetLocalMap() :
	NPCFunction(L"GetLocalMap", &GetLocalMap)
{
}

void* CNPC_GetLocalMap::Call(void *caller, void **params)
{
	return reinterpret_cast<void*(*)(void*, void*, void*)>(functionPtr.functionPtr)(
		caller, params[0], params[1]);
}

void CNPC_GetLocalMap::SetTypes()
{
	SetReturnType(Type::TYPE_INT);
	AddParameter(Type::TYPE_INT);
	AddParameter(Type::TYPE_INT);
}

int CNPC_GetLocalMap::GetLocalMap(CNPC *npc, int mapId, int key)
{
	ScopedLock lock(npc->ext.localMapCS);
	std::map<int, std::map<int, int> >::const_iterator i = npc->ext.localMap.find(mapId);
	if (i == npc->ext.localMap.end()) return 0;
	std::map<int, int>::const_iterator j = i->second.find(key);
	if (j == i->second.end()) return 0;
	return j->second;
}

