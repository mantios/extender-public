
#pragma once

#include <map>
#include <string>

class StaticRespawn {
public:
	class StaticRespawnDef {
	public:
		StaticRespawnDef();

		long minimumDelaySeconds;
		long respawnTimeHour;
		long respawnTimeMinute;
		long respawnTimeRandomSeconds;
	};

	static void Load();

	static std::map<std::wstring, StaticRespawnDef> defs;
};

