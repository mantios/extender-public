
#pragma once

#include <NPCd/CNPCEvent.h>

class CNPCEventFactory {
public:
	static CNPCEvent* Create();
	static CNPCEvent* CreateIsToggleSkillOnOff(CNPC *npc, CSharedCreatureData *target, bool onOff, int skillId);
};

