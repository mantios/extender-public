
#include <Cached/CItem.h>
#include <Cached/CWareHouse.h>
#include <Common/Utils.h>

namespace Cached {

void CItem::Init()
{
	WriteInstructionCallJmpEax(0x47A749, FnPtr(CommitTransactionHelper1));
	WriteInstructionCallJmpEax(0x47AE51, FnPtr(CommitTransactionHelper2));
}

UINT64 CItem::CommitTransactionHelper1(const int warehouseNo)
{
	switch (warehouseNo) {
	case CWareHouse::INVENTORY:
	case CWareHouse::WAREHOUSE:
	case CWareHouse::MAIL:
		return 0x47ABC8;
	default:
		return 0x47A78E;
	}
}

UINT64 CItem::CommitTransactionHelper2(const int warehouseNo)
{
	switch (warehouseNo) {
	case CWareHouse::INVENTORY:
	case CWareHouse::WAREHOUSE:
	case CWareHouse::MAIL:
		return 0x47B355;
	default:
		return 0x47AE96;
	}
}

INT64 CItem::Amount() const
{
	return reinterpret_cast<INT64(*)(const CItem*)>(0x47747C)(this);
}

void CItem::SetAmount(INT64 amount)
{
	reinterpret_cast<void(*)(CItem*, INT64)>(0x476444)(this, amount);
}

void CItem::SetWarehouse(int warehouse)
{
	reinterpret_cast<void(*)(CItem*, int)>(0x477720)(this, warehouse);
}

bool CItem::Save(bool force)
{
	return reinterpret_cast<bool(*)(CItem*, bool)>(0x478770)(this, force);
}

SmartPtr<CItem> CItem::Copy()
{
	SmartPtr<CItem> ret;
	reinterpret_cast<void(*)(CItem*, SmartPtr<CItem>*)>(0x478A5C)(this, &ret);
	return ret;
}

void CItem::SetOwnerDBID(int id)
{
	ownerDBID = id;
}

int CItem::GetDBID()
{
	return reinterpret_cast<int(*)(CItem*)>(0x47735C)(this);
}

SmartPtr<CItem> CItem::SetTransaction()
{
	SmartPtr<CItem> ret;
	reinterpret_cast<void(*)(CItem*, SmartPtr<CItem>*)>(0x477F04)(this, &ret);
	return ret;
}

void CItem::SetTransactMode(int mode)
{
	reinterpret_cast<void(*)(CItem*, int)>(0x477788)(this, mode);
}

void CItem::CommitTransaction(CItem *copy, bool unknown)
{
	reinterpret_cast<void(*)(CItem*, CItem*, bool)>(0x47A624)(this, copy, unknown);
}

void CItem::RollbackTransaction(CItem *copy)
{
	reinterpret_cast<void(*)(CItem*, CItem*)>(0x479920)(this, copy);
}

int CItem::ItemType()
{
	return reinterpret_cast<int(*)(CItem*)>(0x47741C)(this);
}

void CItem::SetInventorySlotIndex(int index)
{
	reinterpret_cast<void(*)(CItem*, int)>(0x47835C)(this, index);
}

int CItem::GetConsumeType()
{
	return reinterpret_cast<int(*)(UINT64, int)>(0x47B9FC)(0x1962570, ItemType());
}

bool CItem::IsStackable()
{
	switch (GetConsumeType()) {
	case 2:
	case 3:
		return true;
	default:
		return false;
	}
}

void CItem::TransactDelete()
{
	reinterpret_cast<void(*)(CItem*)>(0x4772DC)(this);
}

CompileTimeOffsetCheck(CItem, ownerDBID, 0x20);
CompileTimeOffsetCheck(CItem, amount, 0x28);

} // namespace Cached

