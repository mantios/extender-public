
#pragma once

namespace Cached {

class CSocket {
public:
	/* 0x0000 */ virtual ~CSocket();
	/* 0x0008 */ virtual void vfn1() {}
	/* 0x0010 */ virtual void vfn2() {}
	/* 0x0018 */ virtual void vfn3() {}
	/* 0x0020 */ virtual void vfn4() {}
	/* 0x0028 */ virtual void vfn5() {}
	/* 0x0030 */ virtual void vfn6() {}
	/* 0x0038 */ virtual void vfn7() {}
	/* 0x0040 */ virtual void vfn8() {}
	/* 0x0048 */ virtual void vfn9() {}
	/* 0x0050 */ virtual void vfn10() {}
	/* 0x0058 */ virtual void vfn11() {}
	/* 0x0060 */ virtual void vfn12() {}
	/* 0x0068 */ virtual void Send(const char *format, ...) {}
};

} // namespace Cached

