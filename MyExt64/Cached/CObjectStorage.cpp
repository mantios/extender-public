
#include <Cached/CObjectStorage.h>
#include <Cached/CWareHouse.h>
#include <Common/CLog.h>

namespace Cached {

template<>
void CObjectStorage<CWareHouse>::SetObject(CWareHouse *object)
{
	reinterpret_cast<void(*)(CObjectStorage<CWareHouse>*, CWareHouse*)>(0x44C8B4)(this, object);
}

template<>
SmartPtr<CWareHouse> CObjectStorage<CWareHouse>::GetObject()
{
	SmartPtr<class CWareHouse> ret;
	reinterpret_cast<void(*)(CObjectStorage<CWareHouse>*, SmartPtr<CWareHouse>*)>(0x44D3A4)(this, &ret);
	return ret;
}

} // namespace Cached

