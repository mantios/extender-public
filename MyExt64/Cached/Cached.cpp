
#include <Cached/Cached.h>
#include <Cached/CWareHouse.h>
#include <Cached/CUser.h>
#include <Cached/ExtPacket.h>
#include <Cached/CItem.h>
#include <Common/Utils.h>
#include <Common/CLog.h>

namespace Cached {

void Cached::Init()
{
	DisableSendMail();
	HookStart();
	ChangePaths();
	FixFloatingPoint();
	CWareHouse::Init();
	CUser::Init();
	ExtPacket::Init();
	CItem::Init();
	FixWcstol();
	NOPMemory(0x474958, 6); // NOP out SetThreadAffinityMask
	InitUtf8Support();
}

void Cached::DisableSendMail()
{
	NOPMemory(0x4623C9, 5);
	NOPMemory(0x463066, 5);
}

void Cached::HookStart()
{
	WriteInstructionCall(0x44C28D, reinterpret_cast<UINT32>(StartHook));
	WriteInstructionCall(0x44C258, reinterpret_cast<UINT32>(CreateWindowEx), 0x44C258 + 6);
}

void Cached::ChangePaths()
{
	ReplaceString(0x5ECCE8, L"IMPORT_FILES", L"script");
	ReplaceString(0x5ECE50, L"IMPORT_FILES", L"script");
	ReplaceString(0x5ECF98, L"IMPORT_FILES", L"script");
	ReplaceString(0x62DC28, L"IMPORT_FILES", L"script");
	ReplaceString(0x663B20, L"IMPORT_FILES", L"script");
}

HWND Cached::CreateWindowEx(DWORD dwExStyle, LPCWSTR lpClassName, LPCWSTR lpWindowName, DWORD dwStyle, int X, int Y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
	std::wstring name(lpWindowName);
	name += L" - patched by MyExt64 (https://bitbucket.org/l2shrine/extender-public)";
	return ::CreateWindowEx(dwExStyle, lpClassName, name.c_str(), dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
}

void Cached::StartHook(void *logger, int level, const char *fmt)
{ GUARDED

	reinterpret_cast<void(*)(void*, int, const char*)>(0x47E8AC)(logger, level, fmt);
	CLog::Add(CLog::Blue, L"Patched by MyExt64 (https://bitbucket.org/l2shrine/extender-public)");
	ShellExecute(0, L"open", L"cmd.exe", L"/C mkdir bak", 0, SW_HIDE);
	ShellExecute(0, L"open", L"cmd.exe", L"/C move LinError.txt.*.bak bak\\", 0, SW_HIDE);
}

void Cached::FixFloatingPoint()
{
	WriteInstructionCall(0x5A9797, FnPtr(_vsnwprintf_s_l));
	WriteInstructionCall(0x5A9BFC, FnPtr(_vsnwprintf_s_l));
}

void Cached::InitUtf8Support()
{
	WriteInstructionCall(0x553F56, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x55403C, FnPtr(LoadBinaryAbsolute));
	WriteInstructionCall(0x5A1BAA, FnPtr(ReadFirstWchar));
	WriteInstructionCall(0x5A1C24, FnPtr(ReadNextWchar));
}

void Cached::FixWcstol()
{
	WriteInstructionJmp(0x5A8D16, FnPtr(WcstolFix));
}

} // namespace Cached

