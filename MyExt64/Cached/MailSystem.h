
#pragma once

#include <Common/CriticalSection.h>
#include <vector>

namespace Cached {

class MailSystem {
public:
	class Attachment {
	public:
		static Attachment FromPacket(const unsigned char **packet);

		int itemDbId;
		int itemObjectId;
		INT64 amount;
	};

	class Post {
	public:
		static Post FromPacket(const unsigned char **packet);

		int id;
		int senderDbId;
		int senderObjectId;
		std::wstring sender;
		std::wstring recipient;
		std::wstring subject;
		std::wstring content;
		INT64 codAdena;
		std::vector<Attachment> attachments;
	};

	class PostLocker {
	public:
		class Guard {
		public:
			Guard(PostLocker &locker, const int id);
			~Guard();

		protected:
			PostLocker &locker;
			const int id;
		};

		void Lock(const int id);
		void Unlock(const int id);

	protected:
		CriticalSection cs[16];
	};

	static PostLocker locker;

	static void SendPost(class CSocket *socket, Post &post);
	static void ReceivePostList(class CSocket *socket, const int userDbId, const int userObjectId);
	static void SentPostList(class CSocket *socket, const int userDbId, const int userObjectId);
	static void ReceivedPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id);
	static void SentPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id);
	static void DeleteReceivedPost(class CSocket *socket, const int userDbId, const int userObjectId, const std::vector<int> &ids);
	static void DeleteSentPost(class CSocket *socket, const int userDbId, const int userObjectId, const std::vector<int> &ids);
	static void ReceivePost(class CSocket *socket, const int userDbId, const int userObjectId, const int id);
	static void CheckMail(class CSocket *socket, const int userDbId, const int userObjectId);
	static void RejectPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id);
	static void ReturnPost(class CSocket *socket, const int senderDbId, const int recipientDbId, const int id, const std::vector<int> &items);
	static void CancelPost(class CSocket *socket, const int userDbId, const int userObjectId, const int id);
	static void Upkeep(class CSocket *socket);
};

} // namespace Cached