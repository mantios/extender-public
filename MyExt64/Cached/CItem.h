
#pragma once

#include <Common/SmartPtr.h>

namespace Cached {

class CItem {
public:
	static void Init();

	INT64 Amount() const;
	void SetAmount(INT64 amount);
	void SetWarehouse(int warehouse);
	bool Save(bool force);
	void SetOwnerDBID(int id);
	int GetDBID();
	SmartPtr<CItem> Copy();
	SmartPtr<CItem> SetTransaction();
	void SetTransactMode(int mode);
	void CommitTransaction(CItem *copy, bool unknown);
	void RollbackTransaction(CItem *copy);
	int ItemType();
	void SetInventorySlotIndex(int index);
	int GetConsumeType();
	bool IsStackable();
	void TransactDelete();

	static UINT64 CommitTransactionHelper1(const int warehouseNo);
	static UINT64 CommitTransactionHelper2(const int warehouseNo);

	enum TransactionMode {
		UPDATE = 1,
		CREATE = 2
	};

	/* 0x0000 */ char padding0x0000[0x0020 - 0x0000];
	/* 0x0020 */ int ownerDBID;
	/* 0x0024 */ char padding0x0024[0x0028 - 0x0024];
	/* 0x0028 */ INT64 amount;
	/* 0x0030 */ int enchant;
	/* 0x0034 */ char padding0x0034[0x0082 - 0x0034];
	/* 0x0082 */ INT16 attribute[9];
};

} // namespace Cached

